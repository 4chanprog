; tail-end-recursion.scm
; Simple examples of how to use tail-end recursion in Scheme, and how they are
; much faster than their simpler counterparts.
; Only one example so far, but I'm sure the other /prog/ members can add to it.

(define (fibonacci n)
  (define (fibonacci-acc n a b)
    (if (> n 0)
      (fibonacci-acc (- n 1) b (+ a b))
      a))
  (fibonacci-acc n 0 1))

(define (fibonacci-slow n)
  (cond ((= n 0) 0)
        ((= n 1) 1)
        (else (+ (fibonacci-slow (- n 1))
                 (fibonacci-slow (- n 2))))))
