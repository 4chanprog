; Work in progress...

(define (one-of string)
  (map list (string->list string)))

(define (match pattern x)
  (define (single-match pat x)
    (cond ((procedure? pat) (pat x))
          ((char?      pat) (and (char=? pat (car x))
                                 (cons (car x) (cdr x))))))
  (let loop ((acc (list)) (pattern pattern) (x x))
    (if (null? pattern)
        (cons (reverse acc) x)
        (let ((result (single-match (car pattern) x)))
          (and result
               (loop (cons (car result) acc) (cdr pattern) (cdr result)))))))

(define-syntax do-it
  (syntax-rules ()
    ((_ x) (car x))
    ((_ x f) (apply f x))))

(define-syntax make-parser
  (syntax-rules ()
    ((_ start (rulename (pattern ...) . function) ...)
     (lambda (string)
       (letrec
           ((rulename
             (lambda (x)
               ((lambda (y) (cons (do-it (car y) . function) (cdr y)))
                (cond
                  ((match (list . pattern) x))
                  ...))))
            ...)
         (start (string->list string)))))))

(define test-parser
  (make-parser
   one
   (one   ((#\1 two three)) list)
   (two   ((#\2)) (lambda (x) "SHIT"))
   (three ((#\3)))))

(define (showcons x)
  (display "(")
  (write (car x))
  (display " . ")
  (write (cdr x))
  (display ")"))

(showcons (test-parser "123a"))