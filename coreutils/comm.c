/* @comm.c */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

/* TODO: memory limit on very long lines? */

struct line {
 unsigned char *data;
 unsigned int size;
 unsigned int used;
} l1={0,80}, l2={0,80}; 

char cols=7;

/* returns 1 on error, 0 success/eof (check l->used) */
int lineread(struct line *l, FILE *f) {
 int c;
 unsigned int size=l->size;
 unsigned int used=0;
 unsigned char *data=l->data;
 l->used=0;
 do {
  if(used+1>=size || !data) {
   if(!(data=realloc(data,2*size)))
    return 1;
   size*=2;
  }
  data[used++]=c=fgetc(f);
 } while(c!=-1 && c!='\n');
 if(ferror(f))
  return 1;
 if(c==-1)
  data[--used]=0;
 else
  data[used]=0;
 l->data=data;
 l->size=size;
 l->used=used;
 return 0;
}

int main(int argc, char **argv) {
 FILE *f1, *f2;
 int ret=0,need=3,res;
 int i;

 if(argc<3) {
 usage:
  fprintf(stderr,"usage: %s [-123] file1 file2\n",argv[0]);
  return 1;
 }
 while((i=getopt(argc,argv,"123"))!=-1) {
  switch(i) {
   case '1':
    cols&=6;
    break;
   case '2':
    cols&=5;
    break;
   case '3':
    cols&=3;
    break;
   default:
    goto usage;
  }
 }
 if(argc-optind<2)
  goto usage;
 if(!(f1=fopen(argv[optind++],"rb"))) {
 cant_open:
  fprintf(stderr,"%s: cannot open %s: %s\n",argv[0],argv[optind],strerror(errno));
  return 1;
 }
 if(!(f2=fopen(argv[optind++],"rb")))
  goto cant_open;
 if(!cols)
  goto no_output; /* no output requested ... */
 for(;;){
  if((need&1)&&lineread(&l1,f1)) {
  read_err_1:
   fprintf(stderr,"%s: error reading %s: %s\n",argv[0],argv[optind-1],strerror(errno));
   ret|=1;
   goto no_output;
  }
  if((need&2)&&lineread(&l2,f2)) {
  read_err_2:
   fprintf(stderr,"%s: error reading %s: %s\n",argv[0],argv[optind],strerror(errno));
   ret|=1;
   goto no_output;
  }
  if(!(l1.used)) { /* EOF on file1, out file 2 */
   if(cols&2 && l2.used)
    do {
     if(cols&1) putchar('\t');
     fwrite(l2.data,1,l2.used,stdout);
     if(lineread(&l2,f2))
      goto read_err_2;
    } while(l2.used);
   break;
  }
  if(!(l2.used)) { /* EOF on file2, out file 1*/
   if(cols&1 && l1.used)
    do {
     fwrite(l1.data,1,l1.used,stdout);
     if(lineread(&l1,f1))
      goto read_err_1;
    } while(l1.used);
   break;
  }
  res=memcmp(l1.data,l2.data,(l1.used < l2.used)?l1.used+1:l2.used+1);
  if(!res) { /* line in both files */
   if(cols&4 && l1.used) {
    if(cols&2) putchar('\t');
    if(cols&1) putchar('\t');
    fwrite(l1.data,1,l1.used,stdout);
   }
   need=3;
  } else if(res<0) { /* line only in file1 */
   if(cols&1)
    fwrite(l1.data,1,l1.used,stdout);
   need=1;
  } else { /* line only in file2 */
   if(cols&2) {
    if(cols&1) putchar('\t');
    fwrite(l2.data,1,l2.used,stdout);
   }
   need=2;
  }
 } 
no_output:
 ret|=fclose(f1);
 ret|=fclose(f2);
 free(l1.data);
 free(l2.data);
 return ret;
}
