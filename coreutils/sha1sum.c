/* @sha1sum.c */
typedef unsigned int u32; /* change these to match your compiler's wordsize */
typedef unsigned long long u64;

#include <errno.h>
#include <stdio.h>
#include <string.h>

#define S(X,n) (((X)<<(n))|((X)>>(32-(n))))
#define F1(B,C,D) (((B)&(C))|(~(B)&(D)))
#define F2(B,C,D) ((B)^(C)^(D))
#define F3(B,C,D) (((B)&(C))|((B)&(D))|((C)&(D)))
#define F4(B,C,D) F2(B,C,D)

u32 A,B,C,D,E,H0,H1,H2,H3,H4,T;
u32 W[80];
u64 L;

void sha1init() {
L = 0;
H0 = 0x67452301;
H1 = 0xEFCDAB89;
H2 = 0x98BADCFE;
H3 = 0x10325476;
H4 = 0xC3D2E1F0;
}

void unendian() {
 int i;
 for(i=0;i<16;i++)
  W[i] = ((W[i]&255)<<24) + (((W[i]>>8)&255)<<16) + (((W[i]>>16)&255)<<8) + (W[i]>>24);
}

void sha1round() {
 int i;
 unendian();
 for(i=16;i<80;i++)
  W[i] = S(W[i-3]^W[i-8]^W[i-14]^W[i-16],1);

 A = H0; B = H1; C = H2; D = H3; E = H4;

 for(i=0;i<20;i++) {
  T = S(A,5)+F1(B,C,D) + E + W[i] + 0x5A827999;
  E = D; D = C; C = S(B,30); B = A; A = T;
 }
 for(i=20;i<40;i++) {
  T = S(A,5)+F2(B,C,D) + E + W[i] + 0x6ED9EBA1;
  E = D; D = C; C = S(B,30); B = A; A = T;
 }
 for(i=40;i<60;i++) {
  T = S(A,5)+F3(B,C,D) + E + W[i] + 0x8F1BBCDC;
  E = D; D = C; C = S(B,30); B = A; A = T;
 }
 for(i=60;i<80;i++) {
  T = S(A,5)+F4(B,C,D) + E + W[i] + 0xCA62C1D6;
  E = D; D = C; C = S(B,30); B = A; A = T;
 }
 H0 += A;
 H1 += B;
 H2 += C;
 H3 += D;
 H4 += E;
 L += 64;
}

/* hash last partial block 0..63 bytes
 remainder = 0: append 80 00 00 ... + len and hash
 1<= remainder <= 55: append 80 + len and hash
 56<= remainder <= 64: append 80 and hash, then hash 00 00 + len */
void sha1last(int lastblocksize) {
 *(((unsigned char *)&W)+lastblocksize) = 128;
 memset(((char *)&W)+lastblocksize+1,0,63-lastblocksize);
 if(lastblocksize>55) {
  sha1round();
  L-=64;
  memset(W,0,64);
 }
 L += lastblocksize;
 L *= 8; /* endian-dependent -- I'm assuming little-endian here */
 W[14] = (((L>>32)&255)<<24)+(((L>>40)&255)<<16)+(((L>>48)&255)<<8)+(L>>56);
 W[15] = ((L&255)<<24)+(((L>>8)&255)<<16)+(((L>>16)&255)<<8)+((L>>24)&255);
 sha1round();
 printf("%08x%08x%08x%08x%08x",H0,H1,H2,H3,H4);
}

int sha1file(char *filename) {
 int i;
 FILE *infile = (filename&&strcmp(filename,"-"))?fopen(filename,"rb"):stdin;
 if(!infile) {
  perror(filename);
  return 0;
 }
 sha1init();
 while((i=fread(W,1,64,infile))==64)
  sha1round();
 if(ferror(infile)) {
  perror(filename);
  if(filename)
  fclose(infile);
  return 0;
 } else
  sha1last(i);
 if(filename)
  fclose(infile);
 return 1;
}

int main(int argc, char **argv) {
 int i=1;
 if(argc==1)
  goto sha1_stdin;
 for(;i<argc;i++)
 sha1_stdin:
  if(sha1file(argv[i]))
   printf(" %s\n",argv[i]?argv[i]:"-");
 return 0;
}
