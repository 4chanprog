/* @touch.c */
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <utime.h>
#include <sys/stat.h>
#include <sys/types.h>

/* TODO -t */

int main(int argc, char **argv) {
 int i,am=3,aflag=0,mflag=0,cflag=0,rflag=0,tflag=0;
 struct utimbuf newtime;

 newtime.actime = newtime.modtime = time(0);

 while((i=getopt(argc,argv,"acmr:t:"))!=-1) {
  switch(i) {
  case 'a':
   aflag=1;
   break;
  case 'c':
   cflag=1;
   break;
  case 'm':
   mflag=1;
   break;
  case 'r': {
   struct stat ref_stat;
    if(stat(optarg,&ref_stat)) {
     fprintf(stderr,"%s: cannot stat %s: %s\n",argv[0],optarg,strerror(errno));
     return 1;
    }
    newtime.actime=ref_stat.st_atime;
    newtime.modtime=ref_stat.st_mtime;
    rflag=1;
   }
   break;
  case 't': { /* [[CC]YY]MMDDhhmm[.SS] */

    tflag=1;
   }
   break;
  default:
  usage:
   fprintf(stderr,"usage: %s [-acm] [-r ref_file | -t time] file...\n",argv[0]);
   return 1;
  }
 }
 if(rflag && tflag) {
  fprintf(stderr,"%s: -t and -r are mutually exclusive\n",argv[0]);
  return 1;
 }
 if(aflag || cflag)
  am=(aflag<<2) + cflag;
 if(optind>=argc)
  goto usage;
 for(i=0;optind<argc;optind++) {
  struct utimbuf newfiletime;
  char *path=argv[optind];
  if(access(path,F_OK)) {
   int newfd;
   if(errno!=ENOENT) {
    fprintf(stderr,"%s: cannot determine existence of %s: %s\n",argv[0],path,strerror(errno));
    i=1;
    continue;
   }
   if(cflag)
    continue;
   if((newfd=creat(path,S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH))==-1) {
    fprintf(stderr,"%s: cannot creat() %s: %s\n",argv[0],path,strerror(errno));
    i=1;
    continue;
   }
   close(newfd);
  }
  if(am!=3) {
   struct stat f_stat;
   if(stat(path,&f_stat)) {
    fprintf(stderr,"%s: cannot stat %s: %s\n",argv[0],path,strerror(errno));
    i=1;
    continue;
   }
   if(am&2)
    newfiletime.actime=f_stat.st_atime;
   else
    newfiletime.modtime=f_stat.st_mtime; 
  } else
   memcpy(&newfiletime,&newtime,sizeof(struct utimbuf));
  if(utime(path,&newfiletime)) {
   fprintf(stderr,"%s: cannot touch %s: %s\n",argv[0],path,strerror(errno));
   i=1;
  }
 }
 return i;
}
