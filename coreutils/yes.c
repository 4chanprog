/* @yes.c */

/* NOT POSIX */
#include <stdio.h>

int main(int argc, char *argv[]) {
 const char *str = argc > 1 ? argv[1] : "y";
 for (;;)
  puts(str);
}
