/* @nice.c */
/* NOTE: POSIX `nice` requires that if nice() fails, the exit status should not be affected and
 exec should still be attempted on the utility. The GNU version is not POSIX compliant in that
 it exits immediately if nice() fails. */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv) {
 int i,n=10;

 while((i=getopt(argc,argv,"n:"))!=-1) {
  if(i=='n')
   n=atoi(optarg);
  else {
   fprintf(stderr,"%s: invalid option '%c'\n",argv[0],optopt);
   return 1;
  }
 }
 if(optind==argc) {
  fprintf(stderr,"usage: %s [-n incr] utility [args...]\n",argv[0]);
  return 1;
 }
 errno=0;
 nice(n);
 if(errno)
  fprintf(stderr,"%s: cannot change nice value: %s\n",argv[0],strerror(errno));
 execvp(argv[optind],&argv[optind]);
 fprintf(stderr,"%s: could not exec %s: %s\n",argv[0],argv[optind],strerror(errno));
 if(errno==ENOENT)
  return 127;
 return 126; 
}
