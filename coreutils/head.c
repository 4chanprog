/* @head.c */
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#define BUF_SIZE 4096
char buf[BUF_SIZE];
int main(int argc, char **argv) {
 int opt,lines=10,curlines,readlen=0,writelen;
 int i,fd=0,ret=0;
 while((opt=getopt(argc,argv,"n:"))!=-1) {
  switch(opt) {
   case 'n':
    if(optind<=argc) {
     lines = atoi(optarg);
     break;
    }
   default:
    printf("usage: head [-n num] [file ...]\n");
    return 1;
  }
 }
 if((i=optind)==argc)
  goto head_stdin;
 for(;i<argc;i++) {
  if((fd=open(argv[i],O_RDONLY))==-1) {
   fprintf(stderr,"%s: cannot open `%s' for reading: %s\n",argv[0],argv[i],strerror(errno));
   ret++;
  } else {
   if(argc-optind-1)
    printf("%s==> %s <==\n",(i==optind)?"":"\n",argv[i]);
  head_stdin:
   curlines=0;
   while(curlines<lines && (readlen=read(fd,buf,BUF_SIZE))>0) {
    writelen=0;
    while(writelen<readlen && curlines<lines) {
     if(buf[writelen++]=='\n')
      curlines++;
    }
    if(write(1,buf,writelen)==-1) {
     fprintf(stderr,"%s: error writing to stdout: %s\n",argv[0],strerror(errno));
     ret++;
     break;
    }
   }
   if(readlen==-1) {
    fprintf(stderr,"%s: error reading `%s': %s\n",argv[0],argv[i],strerror(errno));
    ret++;
   }
   close(fd);
  }
 }
 return ret;
}
