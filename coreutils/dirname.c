/* @dirname.c */
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
 int i;

 if(argc!=2) {
  fprintf(stderr,"usage: %s string\n",argv[0]);
  return 1;
 }
 if(!strcmp(argv[1],"//"))
  goto step_6;
 if(!argv[1][0])
  goto single_dot;
 for(i=0;i<strlen(argv[1]);i++)
  if(argv[1][i]!='/')
   goto notslash;
 goto single_slash;
 notslash:
 for(i=strlen(argv[1])-1;argv[1][i]=='/';i--)
  argv[1][i]=0; 
 if(strrchr(argv[1],'/')) {
  for(i=strlen(argv[1])-1;argv[1][i]&&argv[1][i]!='/';i--)
   argv[1][i]=0;
 step_6:
  for(i=strlen(argv[1])-1;argv[1][i]&&argv[1][i]=='/';i--)
   argv[1][i]=0;
  if(argv[1][0])
   printf("%s\n",argv[1]);
  else
  single_slash:  printf("/\n");
 } else
 single_dot:
  printf(".\n");
 return 0;
}
