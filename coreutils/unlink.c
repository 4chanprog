/* @unlink.c */
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
int main(int argc, char **argv) {
 if(argc!=2) {
  fprintf(stderr,"Usage: %s file\n", argv[0]);
  return 1;
 }
 if(unlink(argv[1])) {
  fprintf(stderr,"%s: could not unlink: %s\n",argv[0],strerror(errno));
  return 1;
 }
 return 0;
}
