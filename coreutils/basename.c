/* @basename.c */
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
 int i;
 char *j, *pos=argv[1];

 if(argc<2 || argc>3) {
  fprintf(stderr,"usage: %s string [suffix]\n",argv[0]);
  return 1;
 }
 if(strlen(argv[1])) {
  for(i=0;i<strlen(argv[1]);i++)
   if(argv[1][i]!='/')
    goto notslash;
  printf("/\n");
  return 0;
 notslash:
  for(i=strlen(argv[1])-1;argv[1][i]=='/';i--)
   argv[1][i]=0; 
  if(pos=strrchr(argv[1],'/'))
   pos++;
  else
   pos=argv[1];
  if(argc==3 && strcmp(pos,argv[2]) && strlen(argv[2]))
   for(i=strlen(argv[2])-1,j=pos+strlen(pos)-1;argv[2][i]==*j;i--,j--)
    *j=0;
 }
 printf("%s\n",pos);
 return 0;
}
