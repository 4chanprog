/* @ln.c */

/* NOTE: POSIX specifies that when a link is created of a symlink, said link
 must be a link to the object that the symlink references. Such is the behavior
 of this implementation.
 GNU ln fails to cover this case, creating another link of the symlink itself
 (which can easily be accomplished with the `link' utility.) */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

int fflag,sflag;
char *name;
struct stat f_stat;

int do_link(char *path1, char *path2) {
 struct stat f_stat2;
 int is_symlink=0;

 if(sflag) {
  if(symlink(path1,path2)) {
   if(fflag && errno==EEXIST) { /* remove existing file and try again */
    if(unlink(path2)) {
    cant_unlink:
     fprintf(stderr,"%s: cannot unlink %s: %s\n",name,path2,strerror(errno));
     return 1;
    } 
    if(!symlink(path1,path2))
     return 0;
   }
   fprintf(stderr,"%s: cannot create symlink: %s\n",name,strerror(errno));
   return 1;
  }
  return 0;
 } else { /* hard link: check file first */
  if(lstat(path1,&f_stat2))
   goto cant_stat;
  if(S_ISLNK(f_stat2.st_mode)) { /* use contents of symlink as path1 */
   char *symlinkbuf=malloc(64);
   int slb_size=64,slb_idx;
   if(!symlinkbuf) {
    fprintf(stderr,"%s: out of memory\n",name);
    return 1;
   }
   for(;;) {
    char *newbuf;
    if((slb_idx=readlink(path1,symlinkbuf,slb_size-1))==-1) {
     fprintf(stderr,"%s: cannot read symlink %s: %s\n",name,path1,strerror(errno));
    free_o_1:
     free(symlinkbuf);
     return 1;
    }
    if(slb_idx<slb_size-1)
     break;
    if(!(newbuf=realloc(symlinkbuf,slb_size*2))) {
     fprintf(stderr,"%s: out of memory\n",name);
     goto free_o_1;
    } 
    symlinkbuf=newbuf;
    slb_size*=2;
   }
   symlinkbuf[slb_idx]=0;
   path1=symlinkbuf;
   is_symlink=1;
  }
  if(link(path1,path2)) {
   if(fflag && errno==EEXIST) { /* try again (check for same file!) */
    if(stat(path1,&f_stat2)) {
    cant_stat:
     fprintf(stderr,"%s: cannot stat %s: %s\n",name,path1,strerror(errno));
    free_1:
     if(is_symlink)
      free(path1);
     return 1;
    }
    if(f_stat.st_dev == f_stat2.st_dev && f_stat.st_ino == f_stat2.st_ino) {
     fprintf(stderr,"%s: cannot link file with itself\n",name);
     goto free_1;
    }
    if(unlink(path2))
     goto cant_unlink;
    if(!link(path1,path2))
     goto free_0;
   }
   fprintf(stderr,"%s: cannot create link: %s\n",name,strerror(errno));
   goto free_1;
  }
  free_0:
  if(is_symlink)
   free(path1);
  return 0;
 }
}

int main(int argc, char **argv) {
 int c;

 name=argv[0];
 while((c=getopt(argc,argv,"fs"))!=-1) {
  if(c=='f')
   fflag=1;
  else if(c=='s')
   sflag=1;
  else {
  usage:
   fprintf(stderr,"usage: %s [-fs] source_file target_file\n       %s [-fs] source_file ... target_dir\n",name,name);
   return 1;
  }
 }
 if(argc-optind<2)
  goto usage;
 if(stat(argv[argc-1],&f_stat)) {
  if(errno==ENOENT)
   goto single_file;
  fprintf(stderr,"%s: cannot stat %s: %s\n",argv[0],argv[argc-1],strerror(errno));
  return 1;
 } 
 if(S_ISDIR(f_stat.st_mode)) { /* create links in target directory */
  char *dest_name=0;
  int dest_alloced=0;
  int src_idx;
  int ret=0;

  if(!(dest_name=malloc(dest_alloced=strlen(argv[argc-1])+16))) {
  err_oom:
   fprintf(stderr,"%s: out of memory\n",argv[0]);
   return 1;
  }
  strcpy(dest_name,argv[argc-1]);
  dest_name[dest_alloced-16]='/';
  dest_name[src_idx=dest_alloced-15]=0;
  for(;optind<argc-1;optind++) {
   char *src_name;
   if(!(src_name=strrchr(argv[optind],'/')))
    src_name=argv[optind];
   else
    src_name++;
   if(strlen(src_name)+src_idx>=dest_alloced) {
    char *new_dest_name=realloc(dest_name,2*dest_alloced);
    if(!new_dest_name)
     goto err_oom;
    dest_name=new_dest_name;
    dest_alloced*=2;
   } 
   strcpy(&dest_name[src_idx],src_name);
   ret|=do_link(argv[optind],dest_name);
  }
  return ret;
 } else { /* single-file mode */
  single_file:
  if(argc-optind!=2)
   goto usage;
  return do_link(argv[argc-2],argv[argc-1]);
 }
}
