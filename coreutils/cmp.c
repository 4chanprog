/* @cmp.c */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#define CMP_BUFSIZE 65536

char buf1[CMP_BUFSIZE];
char buf2[CMP_BUFSIZE];

int main(int argc, char **argv) {
 int c,lflag=0,sflag=0,ret=0;
 int linenum=1,bytenum=1;
 int l1,l2;
 int i;
 FILE *f1, *f2;
 while((c=getopt(argc,argv,"ls"))!=-1) {
  if(c=='l')
   lflag=1;
  else if(c=='s')
   sflag=1;
  else {
   fprintf(stderr,"%s: unrecognised option '%c'\n",argv[0],optopt);
   return 2;
  }
 }
 if(sflag&&lflag) {
  fprintf(stderr,"%s: s and l options are mutually exclusive\n",argv[0]);
  return 2;
 }
 if(argc-optind<2) {
  fprintf(stderr,"usage: %s [-l | -s] file1 file2\n",argv[0]);
  return 2;
 }
 if(!(f1=fopen(argv[optind++],"rb"))||!(f2=fopen(argv[optind++],"rb"))) {
  fprintf(stderr,"%s: cannot open %s: %s\n",argv[0],argv[optind-1],strerror(errno));
  return 2;
 }
 while((l1=fread(buf1,1,CMP_BUFSIZE,f1))&&(l2=fread(buf2,1,CMP_BUFSIZE,f2))) {
  for(i=0;l1&&l2;i++,l1--,l2--) {
   if(buf1[i]!=buf2[i]) {
    if(!sflag) {
     if(!lflag) {
      printf("%s %s differ: char %d, line %d\n",argv[optind-2],argv[optind-1],bytenum+i,linenum);
     return_differ:
      fclose(f1);
      fclose(f2);
      return 1;
     } else
      printf("%d %o %o\n",bytenum+i,buf1[i],buf2[i]);
    }
    ret=1;
   }
   if(buf1[i]=='\n') linenum++;
  }
  if(l1 && !sflag) {
   printf("cmp: EOF on %s\n",argv[optind-1]);
   goto return_differ;
  } else if(l2 && !sflag) {
   printf("cmp: EOF on %s\n",argv[optind-2]);
   goto return_differ;
  }
  bytenum+=l1;
 }
 fclose(f1);
 fclose(f2);
 return ret; 
}
