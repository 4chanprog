/* @chroot.c */
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* *** This is NOT POSIX! *** */

static char *shell = "/bin/sh";
static char *arg = "-i";

int main(int argc, char **argv) {
    char** args;

    if(argc == 1) {
        fprintf(stderr, "%s: missing operand\n", argv[0]);
        return 1;
    }
    if(argc > 1) {
        if(chroot(argv[1])) {
            fprintf(stderr, "%s: cannot change root directory to %s: %s\n",
                argv[0], argv[1], strerror(errno));
            return 1;
        }
        chdir("/");
    }
    if(argc > 2)
        args = argv + 2;
    else {
        if((args = (char**)malloc(3 * sizeof(char**))) == NULL) {
            fprintf(stderr, "%s: cannot allocate memory: %s\n",
                argv[0], strerror(errno));
            return 1;
        }
        args[0] = shell;
        args[1] = arg;
        args[2] = NULL;
    }
    if(execvp(args[0], args)) {
        fprintf(stderr, "%s: cannot run command `%s': %s\n",
            argv[0],  args[0], strerror(errno));
        if(args != (argv + 2))
            free(args);
        return 1;
    }
    return 0;
}
