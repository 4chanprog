/* @md5sum.c */

/* TODO: make into C89 if possible. */

typedef unsigned int u32; /* change these to match your compiler's wordsize */
typedef unsigned long long u64;

#include <errno.h>
#include <stdio.h>
#include <string.h>

#define S(X,n) (((X)<<(n))|((X)>>(32-(n))))
#define F(x,y,z) ((z)^((x)&((y)^(z))))
#define G(x,y,z) ((y)^((z)&((x)^(y))))
#define H(x,y,z) ((x)^(y)^(z))
#define I(x,y,z) ((y)^((x)|(~(z))))

#define FF(a,b,c,d,x,s,ac) (a) = (b)+S((a) + F((b),(c),(d))+(x)+(ac),s);
#define GG(a,b,c,d,x,s,ac) (a) = (b)+S((a) + G((b),(c),(d))+(x)+(ac),s);
#define HH(a,b,c,d,x,s,ac) (a) = (b)+S((a) + H((b),(c),(d))+(x)+(ac),s);
#define II(a,b,c,d,x,s,ac) (a) = (b)+S((a) + I((b),(c),(d))+(x)+(ac),s);

#define LE(x) ((x)>>24)+((((x)>>16)&255)<<8)+((((x)>>8)&255)<<16)+(((x)&255)<<24)

u32 A,B,C,D,AA,BB,CC,DD;
u32 W[16];
u64 L;

void md5init() {
 L = 0;
 A = 0x67452301;
 B = 0xefcdab89;
 C = 0x98badcfe;
 D = 0x10325476;
}

void md5round() {

 AA = A; BB = B; CC = C; DD = D;

 FF (A, B, C, D, W[ 0], 7, 0xd76aa478);
 FF (D, A, B, C, W[ 1], 12, 0xe8c7b756);
 FF (C, D, A, B, W[ 2], 17, 0x242070db);
 FF (B, C, D, A, W[ 3], 22, 0xc1bdceee);
 FF (A, B, C, D, W[ 4], 7, 0xf57c0faf);
 FF (D, A, B, C, W[ 5], 12, 0x4787c62a);
 FF (C, D, A, B, W[ 6], 17, 0xa8304613);
 FF (B, C, D, A, W[ 7], 22, 0xfd469501);
 FF (A, B, C, D, W[ 8], 7, 0x698098d8);
 FF (D, A, B, C, W[ 9], 12, 0x8b44f7af);
 FF (C, D, A, B, W[10], 17, 0xffff5bb1);
 FF (B, C, D, A, W[11], 22, 0x895cd7be);
 FF (A, B, C, D, W[12], 7, 0x6b901122);
 FF (D, A, B, C, W[13], 12, 0xfd987193);
 FF (C, D, A, B, W[14], 17, 0xa679438e);
 FF (B, C, D, A, W[15], 22, 0x49b40821);

 GG (A, B, C, D, W[ 1], 5, 0xf61e2562);
 GG (D, A, B, C, W[ 6], 9, 0xc040b340);
 GG (C, D, A, B, W[11], 14, 0x265e5a51);
 GG (B, C, D, A, W[ 0], 20, 0xe9b6c7aa);
 GG (A, B, C, D, W[ 5], 5, 0xd62f105d);
 GG (D, A, B, C, W[10], 9, 0x02441453);
 GG (C, D, A, B, W[15], 14, 0xd8a1e681);
 GG (B, C, D, A, W[ 4], 20, 0xe7d3fbc8);
 GG (A, B, C, D, W[ 9], 5, 0x21e1cde6);
 GG (D, A, B, C, W[14], 9, 0xc33707d6);
 GG (C, D, A, B, W[ 3], 14, 0xf4d50d87);
 GG (B, C, D, A, W[ 8], 20, 0x455a14ed);
 GG (A, B, C, D, W[13], 5, 0xa9e3e905);
 GG (D, A, B, C, W[ 2], 9, 0xfcefa3f8);
 GG (C, D, A, B, W[ 7], 14, 0x676f02d9);
 GG (B, C, D, A, W[12], 20, 0x8d2a4c8a);

 HH (A, B, C, D, W[ 5], 4, 0xfffa3942);
 HH (D, A, B, C, W[ 8], 11, 0x8771f681);
 HH (C, D, A, B, W[11], 16, 0x6d9d6122);
 HH (B, C, D, A, W[14], 23, 0xfde5380c);
 HH (A, B, C, D, W[ 1], 4, 0xa4beea44);
 HH (D, A, B, C, W[ 4], 11, 0x4bdecfa9);
 HH (C, D, A, B, W[ 7], 16, 0xf6bb4b60);
 HH (B, C, D, A, W[10], 23, 0xbebfbc70);
 HH (A, B, C, D, W[13], 4, 0x289b7ec6);
 HH (D, A, B, C, W[ 0], 11, 0xeaa127fa);
 HH (C, D, A, B, W[ 3], 16, 0xd4ef3085);
 HH (B, C, D, A, W[ 6], 23, 0x04881d05);
 HH (A, B, C, D, W[ 9], 4, 0xd9d4d039);
 HH (D, A, B, C, W[12], 11, 0xe6db99e5);
 HH (C, D, A, B, W[15], 16, 0x1fa27cf8);
 HH (B, C, D, A, W[ 2], 23, 0xc4ac5665);

 II (A, B, C, D, W[ 0], 6, 0xf4292244);
 II (D, A, B, C, W[ 7], 10, 0x432aff97);
 II (C, D, A, B, W[14], 15, 0xab9423a7);
 II (B, C, D, A, W[ 5], 21, 0xfc93a039);
 II (A, B, C, D, W[12], 6, 0x655b59c3);
 II (D, A, B, C, W[ 3], 10, 0x8f0ccc92);
 II (C, D, A, B, W[10], 15, 0xffeff47d);
 II (B, C, D, A, W[ 1], 21, 0x85845dd1);
 II (A, B, C, D, W[ 8], 6, 0x6fa87e4f);
 II (D, A, B, C, W[15], 10, 0xfe2ce6e0);
 II (C, D, A, B, W[ 6], 15, 0xa3014314);
 II (B, C, D, A, W[13], 21, 0x4e0811a1);
 II (A, B, C, D, W[ 4], 6, 0xf7537e82);
 II (D, A, B, C, W[11], 10, 0xbd3af235);
 II (C, D, A, B, W[ 2], 15, 0x2ad7d2bb);
 II (B, C, D, A, W[ 9], 21, 0xeb86d391);

 A += AA; B += BB; C += CC; D += DD;
 L += 64;
}

/* hash last partial block 0..63 bytes
 remainder = 0: append 80 00 00 ... + len and hash
 1<= remainder <= 55: append 80 + len and hash
 56<= remainder <= 64: append 80 and hash, then hash 00 00  + len */
void md5last(int lastblocksize) {
 *(((unsigned char *)&W)+lastblocksize) = 128;
 memset(((char *)&W)+lastblocksize+1,0,63-lastblocksize);
 if(lastblocksize>55) {
  md5round();
  L-=64;
  memset(W,0,64);
 }
 L += lastblocksize;
 L *= 8;   /* endian-dependent -- I'm assuming little-endian here */
 W[15] = L>>32;
 W[14] = L;
 md5round(); /* hash last block with length */
 printf("%08x%08x%08x%08x",LE(A),LE(B),LE(C),LE(D));
}

int md5file(char *filename) {
 int i;
 FILE *infile = (filename&&strcmp(filename,"-"))?fopen(filename,"rb"):stdin;
 if(!infile) {
  perror(filename);
  return 0;
 }
 md5init();
 while((i=fread(W,1,64,infile))==64)
  md5round();
 if(ferror(infile)) {
  perror(filename);
  if(filename)
   fclose(infile);
  return 0;
 } else
  md5last(i);
 if(filename)
  fclose(infile);
 return 1;
}

int main(int argc, char **argv) {
 int i=1;
 if(argc==1)
  goto md5_stdin;
 for(;i<argc;i++)
  md5_stdin:
  if(md5file(argv[i]))
   printf("  %s\n",argv[i]?argv[i]:"-");
 return 0;
}
