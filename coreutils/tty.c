/* @tty.c */
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* *** -s option was removed in POSIX ***
   *** needs fixing to use isatty() *** */
int main(int argc, char *argv[]) {
 char *tty = ttyname(0);
 if (argc <= 1)
  puts(tty ?: "not a tty");
 else if (argc != 2 || strcmp(argv[1], "-s")) {
  fputs("usage: tty [-s]\n", stderr);
  return 2;
 }
 return !tty;
}
