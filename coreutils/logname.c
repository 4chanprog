/* @logname.c */
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

int main(int argc, char **argv) {
 char *name;
 if(name=getlogin())
  printf("%s\n",name);
 else
  fprintf(stderr,"%s: could not get login name: %s\n",argv[0],strerror(errno));
 return !name;
}
