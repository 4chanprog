/* @sleep_nonposix.c */

/* Non-POSIX sleep utility. Keep this in anoncoreutils/move/delete/etc? */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int getsuffix(char *val)
{
    char c = val[strlen(val)-1];
    switch(c) {
        case 'd': return 86400; break;      /* Days */
        case 'h': return 3600; break;       /* Hours */
        case 'm': return 60; break;         /* Minutes */
        case 's': return 1; break;          /* Seconds */
        default:
            if(c >= '0' && c <= '9')
                return 1;
            break;
    }
    return -1;
}

int main(int argc, char **argv)
{
    int i, mult;
    for(i = 1; i < argc; i++) {
        mult = getsuffix(argv[i]);
        if(mult > 0)
            sleep(atoi(argv[i]) * mult);
        else
            printf("%s: invalid time interval `%s'\n", argv[0], argv[i]);
    }
    return 0;
}
