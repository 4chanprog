/* @mkdir.c */
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

/* FIXME: POSIX symbolic modes */

int dectooct(mode_t mode) {
    mode_t ret = 0;
    int add = mode % 10;

    if(add > 7)
        return -1;
    else
        ret += add;

    add = ((mode / 10) % 10);
    if(add > 7)
        return -1;
    else
        ret += add << 3;

    add = ((mode / 100) % 10);
    if(add > 7)
        return -1;
    else 
        ret += add << 6;
    return ret;
}

int mkd(char *prog, char *dir, mode_t mode) {
    if(mkdir(dir, mode) == -1) {
        fprintf(stderr, "%s: cannot create directory `%s': %s\n",
            prog, dir, strerror(errno));
        return 0;
    }
    return 1;
}

int main(int argc, char **argv) {
    int opt, i, offs = 0;
    int parent = 0;
    mode_t mode = 0777 & ~umask(0);

    while((opt = getopt(argc, argv, "m:p")) != -1) {
        switch(opt) {
            case 'p': 
                parent = 1;
                break;
            case 'm':
                mode = dectooct(atoi(optarg));
                break; 
            case '?':
                return 1;
        }
    }
    
    if(mode == -1) {
        fprintf(stderr, "%s: invalid mode\n", argv[0]);
        return 1;
    }

    if(optind >= argc) {
        fprintf(stderr, "%s: missing operand\n", argv[0]);
        return 1;
    }

    for(i = optind; i < argc; i++) {
        while(argv[i][strlen(argv[i]) - 1] == '/')
            argv[i][strlen(argv[i]) - 1] = 0;
        if(parent) {
            for(offs = 0; offs < strlen(argv[i]); offs++) {
            if(argv[i][offs] == '/') {
                    argv[i][offs] = 0;
                    mkd(argv[0], argv[i], mode);
                    argv[i][offs] = '/';
                }
            }
        }
        mkd(argv[0], argv[i], mode);
    }
    return 0;
}
