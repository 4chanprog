/* @sleep.c */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv) {
 if(argc!=2) {
  fprintf(stderr,"usage: %s time\n",argv[0]);
  return 1;
 }
 sleep(atoi(argv[1]));
 return 0;
}
