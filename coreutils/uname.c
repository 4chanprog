/* @uname.c */
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/utsname.h>
#define SYSNAME 0x01
#define NODNAME 0x02
#define RELEASE 0x04
#define VERSION 0x08
#define MACHINE 0x10
int main(int argc, char **argv) {
    struct utsname name;
    int options = SYSNAME;
    int opt;

    if(uname(&name) == -1) {
        fprintf(stderr, "%s: internal error: %s\n",
            argv[0], strerror(errno));
        return 1;
    }

    while((opt = getopt(argc, argv, "asnrvm")) != -1) {
        switch(opt) {
            case 'a':
                options |= SYSNAME;
                options |= NODNAME;
                options |= RELEASE;
                options |= VERSION;
                options |= MACHINE;
                break;
            case 's': options |= SYSNAME; break;
            case 'n': options |= NODNAME; break;
            case 'r': options |= RELEASE; break;
            case 'v': options |= VERSION; break;
            case 'm': options |= MACHINE; break;
            case '?':
                return 1;
                break;
        }
    }
    if(options & SYSNAME)
        printf("%s ", name.sysname);
    if(options & NODNAME)
        printf("%s ", name.nodename);
    if(options & RELEASE)
        printf("%s ", name.release);
    if(options & VERSION)
        printf("%s ", name.version);
    if(options & MACHINE)
        printf("%s", name.machine);
    printf("\n");
    return 0;
}
