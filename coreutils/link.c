/* @link.c */
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
int main(int argc, char **argv) {
 if(argc<3) {
  printf("Usage: %s file1 file2\n", argv[0]);
  return 1;
 }
 if(link(argv[1],argv[2])) {
  fprintf(stderr,"%s: could not create link: %s\n",argv[0],strerror(errno));
  return 1;
 }
 return 0;
}
