/* @chown.c */
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <grp.h>
#include <pwd.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int opt_h,opt_R,opt_HLP;
gid_t new_gid=-1;
uid_t new_uid;

char *g_nam;

struct visited {
 struct visited *next;
 dev_t dev;
 ino_t ino;
} *v_stack;

int add_visited(struct stat *s) {
 struct visited *v;
 if(!(v=malloc(sizeof(struct visited))))
  return 1;
 v->dev = s->st_dev;
 v->ino = s->st_ino;
 v->next = v_stack;
 if(v_stack)
  v_stack->next=v;
 else
  v_stack=v;
 return 0;
}

int del_visited() {
 if(v_stack) {
  struct visited *v = v_stack->next;
  free(v_stack);
  v_stack=v;
  return 0;
 }
 return 1;
}

int is_visited(struct stat *s) {
 struct visited *v=v_stack;
 while(v) {
  if(v->dev==s->st_dev && v->ino==s->st_ino)
   return 1;
  v=v->next;
 }
 return 0;
}

/* chown everything in directory path (and recurse, per opt_R and opt_HLP) */
int do_chown(char *path) {
 DIR *cur;
 struct dirent* cur_file;
 struct stat f_stat;
 char *filename;
 int ret=0;

 if(stat(path,&f_stat)) {
  fprintf(stderr,"%s: cannot stat %s: %s\n",g_nam,path,strerror(errno));
  return 1;
 }
 if(chdir(path)) {
  fprintf(stderr,"%s: cannot enter directory %s\n",g_nam,path);
  return 1;
 }
 if(add_visited(&f_stat)) {
  fprintf(stderr,"%s: cannot add path to stack: %s\n",g_nam,strerror(errno));
  return 1;
 }
 if(!(cur=opendir("."))) {
  fprintf(stderr,"%s: cannot open directory %s\n",g_nam,path);
  ret=1;
  goto enter_parent;
 }
 while(cur_file=readdir(cur)) {
  filename=cur_file->d_name;
  if(!strcmp(filename,".")||!strcmp(filename,".."))
   continue;
  if((opt_h)?lchown(filename,new_uid,new_gid):chown(filename,new_uid,new_gid)) {
   fprintf(stderr,"%s: cannot change owner of %s: %s\n",g_nam,filename,strerror(errno));
   ret=1;
   continue;
  }
  if(opt_R) {
   if(lstat(filename,&f_stat)) {
   stat_fail:
    fprintf(stderr,"%s: cannot stat %s: %s\n",g_nam,filename,strerror(errno));
    ret=1;
    continue;
   }
   switch(f_stat.st_mode&S_IFMT) {
   case S_IFLNK:
    if(stat(filename,&f_stat))
     goto stat_fail;
    if(f_stat.st_mode!=S_IFDIR)
     break;
   case S_IFDIR:
    if(opt_HLP<2 || is_visited(&f_stat))
     break;
    ret|=do_chown(filename);
    break;
   default:
    break;
   }
  }
 }
 ret|=closedir(cur);
 enter_parent:
 del_visited();
 if(chdir("..")) {
  fprintf(stderr,"%s: cannot return to parent of %s: %s\n",g_nam,path,strerror(errno));
  return 1;
 }
 return ret;
}

int main(int argc, char **argv) {
 int c;
 struct group *new_group;
 struct passwd *new_user;
 struct stat f_stat;
 char *path;
 char *group_offs;

 g_nam=argv[0];
 while((c=getopt(argc,argv,"hHLPR"))!=-1) {
  switch(c) {
   case 'h': /* chown symlinks to reg. files */
    opt_h=1;
    break;
   case 'H':
    opt_HLP=1;
    break;
   case 'L':
    opt_HLP=2;
    break;
   case 'P':
    opt_HLP=0;
    break;
   case 'R':
    opt_R=1;
    break;
   case '?':
    fprintf(stderr,"%s: invalid option %c\n",argv[0],optopt);
    goto do_usage;
  }
 }
 if(!argv[optind]) {
  do_usage:
  fprintf(stderr,"usage: %s [-hR] owner[:group] file ...\n       %s -R [-H|-L|-P] owner[:group] file ...\n",g_nam,g_nam);
  return 1;
 }
 group_offs=strchr(argv[optind],':');
 if(group_offs) {
  *group_offs=0;
  group_offs++;
  if(!isdigit(*group_offs)) {
   if(!(new_group=getgrnam(group_offs))) {
    fprintf(stderr,"%s: invalid group %s\n",argv[0],group_offs);
    return 3;
   }
   new_gid=new_group->gr_gid;
  } else
   new_gid=atoi(group_offs);
 }
 if(!isdigit(argv[optind][0])) {
  if(!(new_user=getpwnam(argv[optind++]))) {
   fprintf(stderr,"%s: invalid user %s\n",argv[0],argv[optind-1]);
   return 2;
  }
  new_uid=new_user->pw_uid;
 } else
  new_uid=atoi(argv[optind++]);
 c=0;
 while(path=argv[optind++]) {
  if((opt_h)?lchown(path,new_uid,new_gid):chown(path,new_uid,new_gid)) {
   fprintf(stderr,"%s: cannot change owner of %s: %s\n",argv[0],path,strerror(errno));
   c=1;
   continue;
  }
  if(opt_R) {
   if(lstat(path,&f_stat)) {
   stat_fail:
    fprintf(stderr,"%s: cannot stat %s: %s\n",argv[0],path,strerror(errno));
    c=1;
    continue;
   }
   switch(f_stat.st_mode&S_IFMT) {
   case S_IFLNK:
    if(!opt_HLP)
     break;
    if(stat(path,&f_stat))
     goto stat_fail;
    if(f_stat.st_mode!=S_IFDIR)
     break;
   case S_IFDIR:
    c|=do_chown(path);
    break;
   default:
    break;
   }
  }
 }
 return c;
}
