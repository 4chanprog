/* @rmdir.c */
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

int main(int argc, char **argv) {
 int i,pflag=0,ret=0;

 while((i=getopt(argc,argv,"p"))!=-1) {
  if(i=='p')
   pflag=1;
  else {
   fprintf(stderr,"%s: invalid option `%c'\n",argv[0],optopt);
   return 1;
  }
 }
 if(optind>=argc) {
  fprintf(stderr,"usage: %s [-p] dir...\n",argv[0]);
  return 1;
 }
 for(;optind<argc;optind++) {
  char *slashpos;
  if(rmdir(argv[optind])) {
   fprintf(stderr,"%s: cannot remove %s: %s\n",argv[0],argv[optind],strerror(errno));
   ret=1;
   continue;
  }
  if(pflag) {
   int j;
   for(j=strlen(argv[optind])-1;argv[optind][j]=='/';j--)
    argv[optind][j]=0;
   if(slashpos=strrchr(argv[optind],'/')) {
    *slashpos=0;
    optind--;
   }
  }
 } 
 return ret;
}
