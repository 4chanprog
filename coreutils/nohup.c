/* @nohup.c */
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char **argv) {
 int outfd,tty_1,tty_2;

 if(argc<2) {
  fprintf(stderr,"usage: %s utility [args...]\n",argv[0]);
  return 127;
 }
 tty_1=isatty(1);
 tty_2=isatty(2);
 if(tty_1 || tty_2) {
  outfd=open("nohup.out",O_APPEND|O_CREAT|O_WRONLY,S_IRUSR|S_IWUSR);
  if(outfd==-1) {
   char *filepath;
   char *homedir=getenv("HOME");
   if(!homedir) {
    fprintf(stderr,"%s: cannot get value of $HOME\n",argv[0]);
    return 127;
   }
   if(!(filepath=malloc(strlen(homedir)+11))) {
    fprintf(stderr,"%s: out of memory\n",argv[0]);
    return 127;
   }
   strcpy(filepath,homedir);
   strcat(filepath,"/nohup.out");
   outfd=open(filepath,O_APPEND|O_CREAT|O_WRONLY,S_IRUSR|S_IWUSR);
   free(filepath);
   if(outfd==-1) {
    fprintf(stderr,"%s: cannot create %s/nohup.out\n",argv[0],homedir);
    return 127;
   }
  }
  if(tty_2 && dup2(outfd,2)==-1) {
   fprintf(stderr,"%s: cannot redirect stderr: %s\n",argv[0],strerror(errno));
   return 127;
  }
  if(tty_1 && dup2(outfd,1)==-1) {
   fprintf(stderr,"%s: cannot redirect stdout: %s\n",argv[0],strerror(errno));
   return 127;
  }
 }
 signal(SIGHUP, SIG_IGN);
 execvp(argv[1], argv + 1);
 fprintf(stderr, "%s: cannot exec `%s': %s\n",argv[0],argv[1],strerror(errno));
 if(errno==ENOENT)
  return 127;
 return 126;
}
