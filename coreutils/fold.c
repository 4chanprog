/* @fold.c */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

/* TODO: possible memory limit on huge lines? */

extern char *optarg;
extern int optind,optopt;

int col=0;

char *linebuf;
char *lsp;
char *remain_ptr;
int linebuf_size;
int linebuf_used;
int remain_cnt;

int (*foldgetc)(FILE *)=fgetc;

int fold_get(FILE *f) {
 if(remain_cnt) {
  remain_cnt--;
  return *(remain_ptr++);
 } else
  return fgetc(f);
}

int main(int argc, char **argv) {
 int width = 80, bflag=0, sflag=0, retval=0;
 int i;
 FILE *f;

 while((i=getopt(argc,argv,"bsw:"))!=-1) {
  switch(i) {
  case 'b':
   bflag=1;
   break;
  case 's':
   sflag=1;
   foldgetc=fold_get;
   break;
  case 'w':
   if((width=atoi(optarg))<1) {
    fprintf(stderr,"%s: invalid width %d\n",argv[0],width);
    return 1;
   }
   break;
  case '?':
   fprintf(stderr,"%s: invalid option %c\n",argv[0],optopt);
   return 1;
  }
 }
 if(argc==optind) {
  f=stdin;
  i=argc;
  goto fold_stdin;
 } else
  i=optind;
 while(i<argc) { /* loop for each file */
  int c;
  if(!(f=fopen(argv[i++],"rb"))) {
   fprintf(stderr,"%s: error opening %s: %s\n",argv[0],argv[i-1],strerror(errno));
   retval|=1;
   continue;
  }

 fold_stdin: 
  if(sflag && !linebuf) {
   if(!(linebuf=malloc(80))) {
    fprintf(stderr,"%s: cannot allocate line buffer\n",argv[0]);
    return 1;
   } else
    linebuf_size=80;
  }
  while((c=foldgetc(f))!=EOF) { /* folding loop */
   if(c=='\n') {
    if(sflag && linebuf && linebuf_used) {
     fwrite(linebuf,1,linebuf_used,stdout);
     linebuf_used=0;
     lsp=0; 
    }
    putchar('\n');
    col=0;
    continue;
   }
   if(!bflag) {
    switch(c) {
    case '\t':
     col=(col/8+1)*8;
     break;
    case '\b':
     col=col?col-1:0;
     break;
    case '\r':
     col=0;
     break;
    default:
     col++;
    }
   } else
    col++;
   if(col>width) {
    col=1;
    if(sflag && linebuf) { /* release stored line */
     fwrite(linebuf,1,lsp?(lsp-linebuf+1):linebuf_used,stdout);
     remain_ptr=lsp?lsp+1:0;
     remain_cnt=lsp?linebuf_used-(lsp-linebuf+1):0;
     lsp=0;
     linebuf_used=0;
     ungetc(c,f);
     putchar('\n');
     col=0;
     continue;
    }
    putchar('\n');
   }
   if(sflag) {
    if(linebuf_used>=linebuf_size) {
     char *new_linebuf;
     if(!(new_linebuf=realloc(linebuf,2*linebuf_size))) {
      fprintf(stderr,"%s: not enough memory to process %s\n",argv[0],argv[i-1]);
      retval=1;
      goto outer_continue; 
     }
     linebuf=new_linebuf;
     linebuf_size*=2;
    }
    linebuf[linebuf_used++]=c;
    if(c==' ')
     lsp=linebuf+linebuf_used-1;
   } else
    putchar(c);
  }
 outer_continue:
  if(ferror(f)) {
   fprintf(stderr,"%s: error reading %s: %s\n",argv[0],argv[i-1],strerror(errno));
   retval=1;
  }
  if(sflag) {
   if(linebuf_used)
    retval|=fwrite(linebuf,1,linebuf_used,stdout);
   linebuf_used=0;
   lsp=0;
  }
  if(f!=stdin)
   retval|=fclose(f);
 }
 if(linebuf)
  free(linebuf);
 return retval;
}
