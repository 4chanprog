/* @echo.c */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char **argv) {
 int i=1, no_nl=0, ret=0;

 if(argc>1 && !strcmp(argv[1],"-n")) {
  no_nl=1;
  i++;
 }
 while(i<argc) {
  ret|=(fputs(argv[i++], stdout)==EOF);
  if(i!=argc)
   ret|=(fputc(' ',stdout)==EOF);
 }
 if(!no_nl)
  ret|=(fputc('\n',stdout)==EOF);
 return ret;
}
