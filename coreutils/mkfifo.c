/* @mkfifo.c */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

/* FIXME: POSIX symbolic modes */

int dectooct(mode_t mode) {
 mode_t ret = 0;
 int add = mode % 10;
 if(add > 7)
  return -1;
 else
  ret += add;
 add = ((mode / 10) % 10);
 if(add > 7)
  return -1;
 else
  ret += add * 8;
 add = ((mode / 100) % 10);
 if(add > 7)
  return -1;
 else
  ret += add * 64;
 return ret;
}

int main(int argc, char **argv) {
 mode_t mode = 0666 & ~umask(0);
 int i, opt;
 while ((opt = getopt(argc, argv, "m:")) != -1) {
  switch(opt) {
  case 'm':
   mode = dectooct(atoi(optarg));
   break;
  case '?':
   mode = -1;
   goto outofwhile;
  }
 }
 outofwhile:
 if(mode == -1) {
  fprintf(stderr, "%s: invalid mode\n", argv[0]);
  return 1;
 }
 if(optind >= argc) {
  fprintf(stderr, "%s: missing operand\n", argv[0]);
  return 1;
 }
 for(i = optind; i < argc; i++) {
  if(mkfifo(argv[i], mode)) {
   fprintf(stderr, "%s: cannot create fifo `%s': %s\n",
   argv[0], argv[i], strerror(errno));
  }
 }
 return 0;
}
