/* @whoami.c */

/* NOT POSIX */
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <pwd.h>
int main() {
 struct passwd *p = getpwuid(geteuid());
 if(p)
  printf("%s\n",p->pw_name);
 return 0;
}
