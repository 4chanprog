/* @tee.c */
#include <signal.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#define TEE_BUFSIZE 65536

char buf[TEE_BUFSIZE];

int main(int argc, char** argv) {
  int flags = O_WRONLY | O_CREAT | O_TRUNC, *fd, fdcnt, i, nb;

  while((i = getopt(argc, argv, "ai")) != -1) {
    switch(i) {
    case 'a':
      flags = O_WRONLY | O_CREAT | O_APPEND;
      break;
    case 'i':
      signal(SIGINT, SIG_IGN);
      break;
    case '?':
      printf("usage: tee [-ai] [file ...]\n");
      return 1;
      break;
    }
  }
  fdcnt = argc-optind + 1;
  if(!(fd = malloc(fdcnt * sizeof(int))))
   goto perror_die;
  fd[0] = 1;
  for(i = optind; i < argc; i++)
    if((fd[i-optind+1] = open(argv[i], flags, 0666)) == -1) {
    perror_die:
      perror(NULL);
      return 1;
    }
  while((nb = read(0, buf, 1024)) > 0) {
    for(i = 0; i < fdcnt; i++)
      write(fd[i], buf, nb);
  }
  if(nb == -1)
   goto perror_die;
  return 0;
}
