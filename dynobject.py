## dynobject.py
## a dynamic object for Python
## by Sam Fredrickson <kinghajj@gmail.com>, 2007-02-22
##
## inspired by Javascript. in JS, you can make objects like this:
## o = {a: 2, b: 3}
## o.c = 4
## o.d = 5
## ...
##
## with dynobject, you can do similar in python:
## o = dynobject(a=2, b=3)
## o.c = 4
## o.d = 5

# ah, metaclasses. here's a use for them. without this metaclass, I couldn't
# use setattr() to define new properties on-the-fly
class __dynobject_meta__(type):
	# when you are creating a dynobject, you can pass it keywords
	def __call__(self, **kwargs):
		obj = type.__call__(self)
		for key in kwargs:
			setattr(obj, key, kwargs[key])
		return obj

class dynobject(object):
	__metaclass__ = __dynobject_meta__

	# when you iterate, you get pairs of attribute names and values
	def __iter__(self):
		attrs = dir(self)
		for attr in attrs:
			if attr[0:2] != "__" and attr[-3:-1] != "__":
				yield (attr, getattr(self, attr))

	# if you request a nonexistant attribute, you get None rather than an
	# error
	def __getattribute__(self, attr):
		try:
			return object.__getattribute__(self, attr)
		except:
			return None

